﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace InventarioServices
{
    /// <summary>
    /// Descripción breve de EditorialService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class EditorialService : System.Web.Services.WebService
    {

        [WebMethod]
        public bool AgregarEditorial(string codigo, string nombre, string contacto, string telefono)
        {
            string connetionString = null;
            SqlConnection cnn;

            connetionString = "Data Source=(localdb)\\MSSQLLocalDB;" +
            "Initial Catalog=inventario;Integrated Security=True;";
            cnn = new SqlConnection(connetionString);

            try
            {
                string query = "INSERT INTO editoriales (codigoEditorial, nombreEditorial, contacto, telefono) " +
                            "values(@codigoEditorial, @nombreEditorial, @contacto, @telefono)";

                cnn.Open();
                SqlCommand cmd = new SqlCommand(query, cnn);

                cmd.Parameters.Add("@codigoEditorial", System.Data.SqlDbType.VarChar, 10).Value = codigo;
                cmd.Parameters.Add("@nombreEditorial", System.Data.SqlDbType.VarChar, 150).Value = nombre;
                cmd.Parameters.Add("@contacto", System.Data.SqlDbType.VarChar, 100).Value = contacto;
                cmd.Parameters.Add("@telefono", System.Data.SqlDbType.VarChar, 20).Value = telefono;

                int result = cmd.ExecuteNonQuery();

                cnn.Close();

                if(result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }


            }
            catch
            {
                return false;
            }
        }

        [WebMethod]
        public bool ModificarEditorial(string codigo, string nombre, string contacto, string telefono)
        {
            string connetionString = null;
            SqlConnection cnn;

            connetionString = "Data Source=(localdb)\\MSSQLLocalDB;" +
            "Initial Catalog=inventario;Integrated Security=True;";
            cnn = new SqlConnection(connetionString);

            try
            {
                string query = "UPDATE editoriales " +
                            "SET nombreEditorial = @nombreEditorial, contacto = @contacto, telefono = @telefono " +
                            "WHERE codigoEditorial = @codigoEditorial";

                cnn.Open();
                SqlCommand cmd = new SqlCommand(query, cnn);

                cmd.Parameters.Add("@codigoEditorial", System.Data.SqlDbType.VarChar, 10).Value = codigo;
                cmd.Parameters.Add("@nombreEditorial", System.Data.SqlDbType.VarChar, 150).Value = nombre;
                cmd.Parameters.Add("@contacto", System.Data.SqlDbType.VarChar, 100).Value = contacto;
                cmd.Parameters.Add("@telefono", System.Data.SqlDbType.VarChar, 20).Value = telefono;

                int result = cmd.ExecuteNonQuery();

                cnn.Close();

                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }


            }
            catch
            {
                return false;
            }
        }

        [WebMethod]
        public bool EliminarEditorial(string codigo)
        {
            string connetionString = null;
            SqlConnection cnn;

            connetionString = "Data Source=(localdb)\\MSSQLLocalDB;" +
            "Initial Catalog=inventario;Integrated Security=True;";
            cnn = new SqlConnection(connetionString);

            try
            {
                string query = "DELETE FROM editoriales WHERE codigoEditorial = @codigoEditorial";

                cnn.Open();
                SqlCommand cmd = new SqlCommand(query, cnn);

                cmd.Parameters.Add("@codigoEditorial", System.Data.SqlDbType.VarChar, 10).Value = codigo;

                int result = cmd.ExecuteNonQuery();

                cnn.Close();

                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }


            }
            catch
            {
                return false;
            }
        }

        [WebMethod]
        public List<Editorial> ListarEditorial()
        {
            List<Editorial> lista = new List<Editorial>();
            string connetionString = null;
            SqlConnection cnn;

            connetionString = "Data Source=(localdb)\\MSSQLLocalDB;" +
            "Initial Catalog=inventario;Integrated Security=True;";
            cnn = new SqlConnection(connetionString);

            try
            {
                string query = "SELECT * FROM editoriales";

                cnn.Open();

                SqlCommand cmd = new SqlCommand(query, cnn);

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Editorial ed = new Editorial() {
                        codigo = reader["codigoEditorial"].ToString(),
                        nombre = reader["nombreEditorial"].ToString(),
                        contact = reader["contacto"].ToString(),
                        tel = reader["telefono"].ToString()
                    };
                    lista.Add(ed);
                }

            }
            catch
            {
                lista = new List<Editorial>();
            }

            return lista;
        }

        [WebMethod]
        public Editorial BuscarEditorial(string codigo)
        {
            Editorial editorial = null;
            string connetionString = null;
            SqlConnection cnn;

            connetionString = "Data Source=(localdb)\\MSSQLLocalDB;" +
            "Initial Catalog=inventario;Integrated Security=True;";
            cnn = new SqlConnection(connetionString);

            try
            {
                string query = "SELECT TOP 1 * FROM editoriales WHERE codigoEditorial = @codigoEditorial";

                cnn.Open();

                SqlCommand cmd = new SqlCommand(query, cnn);

                cmd.Parameters.Add("@codigoEditorial", System.Data.SqlDbType.VarChar, 10).Value = codigo;

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Editorial ed = new Editorial()
                    {
                        codigo = reader["codigoEditorial"].ToString(),
                        nombre = reader["nombreEditorial"].ToString(),
                        contact = reader["contacto"].ToString(),
                        tel = reader["telefono"].ToString()
                    };
                    editorial = ed;
                }

            }
            catch
            {
                editorial = null;
            }

            return editorial;
        }

        [WebMethod]
        public List<Editorial> BuscarEditorialPorNombre(string nombre)
        {
            List<Editorial> lista = new List<Editorial>();
            string connetionString = null;
            SqlConnection cnn;

            connetionString = "Data Source=(localdb)\\MSSQLLocalDB;" +
            "Initial Catalog=inventario;Integrated Security=True;";
            cnn = new SqlConnection(connetionString);

            try
            {
                nombre = "%" + nombre + "%";
                string query = "SELECT * FROM editoriales WHERE nombreEditorial LIKE @nombreEditorial";

                cnn.Open();

                SqlCommand cmd = new SqlCommand(query, cnn);

                cmd.Parameters.Add("@nombreEditorial", System.Data.SqlDbType.VarChar, 150).Value = nombre;

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Editorial ed = new Editorial()
                    {
                        codigo = reader["codigoEditorial"].ToString(),
                        nombre = reader["nombreEditorial"].ToString(),
                        contact = reader["contacto"].ToString(),
                        tel = reader["telefono"].ToString()
                    };
                    lista.Add(ed);
                }

            }
            catch(Exception ex)
            {
                lista = new List<Editorial>();
            }

            return lista;
        }

        [WebMethod]
        public List<item> ListarCombo()
        {
            List<item> lista = new List<item>();
            string connetionString = null;
            SqlConnection cnn;

            connetionString = "Data Source=(localdb)\\MSSQLLocalDB;" +
            "Initial Catalog=inventario;Integrated Security=True;";
            cnn = new SqlConnection(connetionString);

            try
            {
                string query = "SELECT * FROM editoriales";

                cnn.Open();

                SqlCommand cmd = new SqlCommand(query, cnn);

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    item ed = new item()
                    {
                        codigo = reader["codigoEditorial"].ToString(),
                        nombre = reader["nombreEditorial"].ToString()
                    };
                    lista.Add(ed);
                }

            }
            catch
            {
                lista = new List<item>();
            }

            return lista;
        }

    }

    public class item
    {
        public string codigo { get; set; }
        public string nombre { get; set; }
    }
}
