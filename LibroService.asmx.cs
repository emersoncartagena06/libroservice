﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace InventarioServices
{
    /// <summary>
    /// Descripción breve de LibroService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class LibroService : System.Web.Services.WebService
    {

        [WebMethod]
        public List<Libro> ListarLibros()
        {
            List<Libro> lista = new List<Libro>();
            string connetionString = null;
            SqlConnection cnn;

            connetionString = "Data Source=(localdb)\\MSSQLLocalDB;" +
            "Initial Catalog=inventario;Integrated Security=True;";
            cnn = new SqlConnection(connetionString);

            try
            {
                string query = "SELECT " +
                    "l.codigoLibro, " +
	                "l.nombreLibro, " +
	                "l.existencia, " +
	                "l.precio, " +
	                "l.codigoAutor, " +
                    "a.nombreAutor, " +
	                "l.codigoEditorial, " +
	                "e.nombreEditorial, "+ 
	                "l.idGenero, " +
	                "g.nombreGenero, " +
	                "l.descripcion " +
                    "FROM " +
                    "libros AS l INNER JOIN  " +
                    "autores AS a ON(l.codigoAutor = a.codigoAutor) INNER JOIN " +
                    "editoriales AS e ON(l.codigoEditorial = e.codigoEditorial) INNER JOIN "+ 
                    "generos AS g ON(l.idGenero = g.idGenero)";

                cnn.Open();

                SqlCommand cmd = new SqlCommand(query, cnn);

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Libro lb = new Libro()
                    {
                        codigoLibro = reader["codigoLibro"].ToString(),
                        nombreLibro = reader["nombreLibro"].ToString(),
                        existencia = int.Parse(reader["existencia"].ToString()),
                        precio = decimal.Parse(reader["precio"].ToString()),
                        autor = new Autor()
                        {
                            codigo = reader["codigoAutor"].ToString(),
                            nombre = reader["nombreAutor"].ToString(),
                        },
                        editorial = new Editorial()
                        {
                            codigo = reader["codigoEditorial"].ToString(),
                            nombre = reader["nombreEditorial"].ToString(),
                        },
                        genero = new genero()
                        {
                            id = int.Parse(reader["idGenero"].ToString()),
                            nombre = reader["nombreGenero"].ToString(),
                        },
                        descripcion = reader["descripcion"].ToString()
                    };
                    lista.Add(lb);
                }

            }
            catch
            {
                lista = new List<Libro>();
            }

            return lista;
        }
    }

    public class Libro
    {
        public string codigoLibro { get; set; }
        public string nombreLibro { get; set; }
        public int existencia { get; set; }
        public decimal precio { get; set; }
        public Autor autor { get; set; }
        public Editorial editorial { get; set; }
        public genero genero { get; set; }
        public string descripcion { get; set; }
    }
    public class Editorial
    {
        public string codigo { get; set; }
        public string nombre { get; set; }
        public string contact { get; set; }
        public string tel { get; set; }
    }
    public class Autor
    {
        public string codigo { get; set; }
        public string nombre { get; set; }
        public string nacionalidad { get; set; }

    }
    public class genero
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string descipcion { get; set; }
    }
}
